Feature: Testing a REST API
  Users should be able to submit POST and GET requests to a web service, 
  to store and retrieve employee records.

  Scenario: Data Upload to a web service
    When a new employee record is sent with name "Frank" and address "123 Main St."
    Then the record should be stored for future retrieval
