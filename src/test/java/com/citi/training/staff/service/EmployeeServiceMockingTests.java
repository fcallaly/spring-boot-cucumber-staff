package com.citi.training.staff.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.citi.training.staff.dao.EmployeeRepo;
import com.citi.training.staff.model.Employee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceMockingTests {

    
    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeRepo mockEmployeeRepo;

    @Test
    public void test_employeeService_save(){
        Employee testEmployee = new Employee();
        testEmployee.setName("Mr. Mock McTesty");
        testEmployee.setAddress("123 Main Street");

        // Tell mockRepo that employeeService is going to call save()
        // when it does return the testEmployee object
        when(mockEmployeeRepo.save(testEmployee)).thenReturn(testEmployee);

        employeeService.save(testEmployee);

        verify(mockEmployeeRepo, times(1)).save(testEmployee);
    }
}
