package com.citi.training.staff.service;

import com.citi.training.staff.model.Employee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTests {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceTests.class);

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void test_save_sanityCheck() {
        LOG.info("Message from logger");

        Employee testEmployee = new Employee();
        testEmployee.setName("Mr. Test McTesty");
        testEmployee.setAddress("123 Main Street");

        employeeService.save(testEmployee);
    }

    @Test
    public void test_findAll_sanityCheck() {
        assert(employeeService.findAll().size() >= 0);
    }
}