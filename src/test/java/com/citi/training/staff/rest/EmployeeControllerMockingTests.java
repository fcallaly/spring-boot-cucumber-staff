package com.citi.training.staff.rest;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import com.citi.training.staff.model.Employee;
import com.citi.training.staff.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerMockingTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private EmployeeService employeeService;

	@Test
	public void test_invalidUrlReturnsNotFound() throws Exception {
		this.mockMvc.perform(get("/v1/non_existing")).andDo(print()).andExpect(status().isNotFound());
	}


	@Test
	public void test_getAllEmployees_returnsFromService() throws Exception {
        String testName = "Mr. MOCK TEST";
        Employee testEmployee = new Employee();
        testEmployee.setName(testName);
        testEmployee.setAddress("123 Mock Street");

        List<Employee> trades = Arrays.asList(testEmployee);

		when(employeeService.findAll()).thenReturn(trades);

		this.mockMvc.perform(get("/v1/employee")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("\"name\":\"" + testName +"\"")));
	}

	@Test
	public void test_addEmployee_callsService() throws Exception {
        String testName = "Mr. MOCK TEST";
        Employee testEmployee = new Employee();
        testEmployee.setName(testName);
        testEmployee.setAddress("123 Mock Street");

		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(testEmployee);

		this.mockMvc.perform(post("/v1/employee").header("Content-Type", "application/json").content(requestJson))
					.andDo(print()).andExpect(status().isCreated());

		verify(employeeService, times(1)).save(any(Employee.class));
	}
}
