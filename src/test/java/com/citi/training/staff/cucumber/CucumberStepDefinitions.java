package com.citi.training.staff.cucumber;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.citi.training.staff.model.Employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CucumberStepDefinitions {

    private final Logger log = LoggerFactory.getLogger(CucumberStepDefinitions.class);

    @Autowired
    private TestRestTemplate restTemplate;

    private Employee testEmployee;

    @When("^a new employee record is sent with name \"(.*?)\" and address \"(.*?)\"$")
    public void saveEmployeeRecord(final String employeeName, final String employeeAddress) {
        testEmployee = new Employee();
        testEmployee.setName(employeeName);
        testEmployee.setAddress(employeeAddress);
        
        log.debug("POSTING Employee Record:" + testEmployee);

        ResponseEntity<Employee> response = restTemplate.postForEntity("/v1/employee",
                                                                       testEmployee,
                                                                       Employee.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        testEmployee.setId(response.getBody().getId());

        return;
    }

    @Then("the record should be stored for future retrieval")
    public void checkRecordWasSaved() {
        log.debug("Verify Record Was Saved");

        ResponseEntity<List<Employee>> findAllResponse = restTemplate.exchange(
                                                        "/v1/employee",
                                                        HttpMethod.GET,
                                                        null,
                                                        new ParameterizedTypeReference<List<Employee>>(){});

        assertEquals(HttpStatus.OK, findAllResponse.getStatusCode());

        log.warn("Retrieved Employee: " + findAllResponse.getBody().get(0));
        return;
    }   
}